package ttrang2301.oci.objectstorage;

import com.oracle.bmc.objectstorage.ObjectStorage;
import com.oracle.bmc.objectstorage.ObjectStorageClient;
import com.oracle.bmc.objectstorage.model.BucketSummary;
import com.oracle.bmc.objectstorage.model.CreateBucketDetails;
import com.oracle.bmc.objectstorage.model.CreatePreauthenticatedRequestDetails;
import com.oracle.bmc.objectstorage.requests.CreateBucketRequest;
import com.oracle.bmc.objectstorage.requests.CreatePreauthenticatedRequestRequest;
import com.oracle.bmc.objectstorage.requests.GetNamespaceRequest;
import com.oracle.bmc.objectstorage.requests.GetObjectRequest;
import com.oracle.bmc.objectstorage.requests.ListBucketsRequest;
import com.oracle.bmc.objectstorage.requests.ListObjectsRequest;
import com.oracle.bmc.objectstorage.responses.CreateBucketResponse;
import com.oracle.bmc.objectstorage.responses.GetNamespaceResponse;
import com.oracle.bmc.objectstorage.responses.GetObjectResponse;
import com.oracle.bmc.objectstorage.responses.ListBucketsResponse;
import com.oracle.bmc.objectstorage.responses.ListObjectsResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ttrang2301.oci.OciProperties;

import java.util.Calendar;
import java.util.stream.Collectors;

public class SdkOciObjectStorageClient {

    private static final ObjectStorage CLIENT;
    public static final String BUCKET_INVOICE_CSV_STAGING = "invoice-csv-staging";
    public static final String BUCKET_MERCHANT_SETTLEMENT_DEV_1 = "merchant-settlement-dev1";
    public static final String BUCKET_MERCHANT_SETTLEMENT_DEV_2 = "merchant-settlement-dev2";
    public static final String BUCKET = BUCKET_MERCHANT_SETTLEMENT_DEV_1;
    public static final String NAMESPACE_NAME = "axbjn4gpueto";

    static {
        CLIENT = ObjectStorageClient
                .builder()
//                .build(InstancePrincipalsAuthenticationDetailsProvider.builder().build());
                .build(OciProperties.AUTHENTICATION_DETAILS_PROVIDER);
        CLIENT.setRegion(com.oracle.bmc.Region.AP_SINGAPORE_1);
    }

    public static void main(String[] args) throws Exception {
//        getNamespace();
//        String name = UUID.randomUUID().toString();
//        System.out.println(name);
//        createBucket(name);
//        listBuckets();
        String bucketName = BUCKET_MERCHANT_SETTLEMENT_DEV_1;
        listObjects(BUCKET);
        String objectName = "pom.xmlpom.xml";

        GetObjectResponse response = CLIENT.getObject(GetObjectRequest.builder()
                .namespaceName(NAMESPACE_NAME)
                .bucketName(bucketName)
                .objectName(objectName)
                .build());
        System.out.println(response);

        createPreAuthenticatedRequest(bucketName, objectName);
        System.out.println("Done");
        System.exit(1);
    }

    private static void createPreAuthenticatedRequest(String bucketName, String objectName) {
        final var timeExpires = Calendar.getInstance();
        timeExpires.add(Calendar.MINUTE, 5);
        final var request = CreatePreauthenticatedRequestRequest.builder()
                .namespaceName(NAMESPACE_NAME)
                .bucketName(bucketName)
                .createPreauthenticatedRequestDetails(CreatePreauthenticatedRequestDetails.builder()
                        .name("DownloadReport")
                        .objectName(objectName)
                        .accessType(CreatePreauthenticatedRequestDetails.AccessType.ObjectRead)
                        .timeExpires(timeExpires.getTime())
                        .build())
                .build();
        final var response = CLIENT.createPreauthenticatedRequest(request);
        System.out.println(response.getPreauthenticatedRequest().getAccessUri());
    }

    private static void listObjects(String bucketName) {
        ListObjectsResponse listObjectsResponse = CLIENT.listObjects(ListObjectsRequest.builder()
                .namespaceName(NAMESPACE_NAME)
                .bucketName(bucketName)
                .build());
        System.out.println(listObjectsResponse.getListObjects().getObjects().size());
        listObjectsResponse.getListObjects().getObjects().forEach(objectSummary -> System.out.println(objectSummary.getName()));
    }

    private static void download() {
//        CreateMultipartUploadResponse response = CLIENT.createMultipartUpload(
//                CreateMultipartUploadRequest.builder()
//                        .bucketName()
//                        .build());
    }

    private static void getNamespace() {
        GetNamespaceResponse namespaceResponse =
                CLIENT.getNamespace(GetNamespaceRequest.builder().build());
        String namespaceName = namespaceResponse.getValue();
        System.out.println(namespaceName);
        System.out.println(namespaceName.equals(OciProperties.NAMESPACE));
    }

    private static void createBucket(String name) {
        CreateBucketResponse createBucketResponse = CLIENT.createBucket(
                CreateBucketRequest.builder()
                        .namespaceName(OciProperties.NAMESPACE)
                        .createBucketDetails(CreateBucketDetails.builder()
                                .compartmentId(OciProperties.COMPARTMENT)
                                .name(name)
                                .build())
                        .build());
        System.out.println(createBucketResponse);
    }

    private static void listBuckets() {
        ListBucketsRequest listBucketsRequest = ListBucketsRequest.builder()
                .namespaceName(OciProperties.NAMESPACE)
                .compartmentId(OciProperties.COMPARTMENT)
                .build();
        ListBucketsResponse response = CLIENT.listBuckets(listBucketsRequest);
        System.out.println(response.getItems().size());
        response.getItems().stream()
                .collect(Collectors.groupingBy(BucketSummary::getCompartmentId))
                .forEach((compartmentId, buckets) -> {
                    System.out.println("=== Compartment " + compartmentId + " <" + buckets.size() + ">");
                    buckets.forEach(bucket -> System.out.println(bucket.getName()));
                });
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    public static final class Region {
        private String key;
        private String name;
    }

}
