package ttrang2301.oci.objectstorage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oracle.bmc.Region;
import com.oracle.bmc.http.client.Method;
import com.oracle.bmc.http.signing.DefaultRequestSigner;
import com.oracle.bmc.http.signing.RequestSigner;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ttrang2301.oci.OciProperties;
import ttrang2301.oci.OciStorageProperties;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

/**
 * https://docs.public.oneportal.content.oci.oraclecloud.com/en-us/iaas/api/#/en/objectstorage/20160918/
 * https://docs.oracle.com/en-us/iaas/Content/API/Concepts/signingrequests.htm#one
 */
public class JdkOciObjectStorageClient {

    private static final RequestSigner REQUEST_SIGNER = DefaultRequestSigner.createRequestSigner(OciProperties.AUTHENTICATION_DETAILS_PROVIDER);
    private static final HttpClient CLIENT = HttpClient.newBuilder().build();
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final Set<String> AUTO_FILLED_HTTP_HEADERS = new HashSet<>(Arrays.asList("connection", "content-length", "expect", "host", "upgrade"));

    public static void main(String[] args) throws Exception {
        getNameSpace();
        final String name = UUID.randomUUID().toString();
        System.out.println(name);
        createBucket(name);
//        listBuckets();
    }

    private static void createBucket(String name) throws IOException, InterruptedException {
        final String resourcePath = String.format("/n/%s/b/", OciProperties.NAMESPACE);
        final URI uri = URI.create(OciStorageProperties.computeServiceHost(Region.ME_JEDDAH_1) + resourcePath);
        final String requestBody = OBJECT_MAPPER.writeValueAsString(new CreateBucketRequest(OciProperties.COMPARTMENT, name));
        final HttpRequest.Builder requestBuilder = HttpRequest.newBuilder(uri).POST(HttpRequest.BodyPublishers.ofString(requestBody));
        REQUEST_SIGNER
                .signRequest(uri, Method.POST.name(), Collections.emptyMap(), requestBody)
                .entrySet().stream()
                .filter(entry -> !AUTO_FILLED_HTTP_HEADERS.contains(entry.getKey().toLowerCase()))
                .forEach(entry -> requestBuilder.header(entry.getKey(), entry.getValue()));
        final HttpResponse<String> response = CLIENT.send(requestBuilder.build(), HttpResponse.BodyHandlers.ofString());
        System.out.println(response.statusCode());
        System.out.println(response.body());
    }

    private static void getNameSpace() throws IOException, InterruptedException {
        final String resourcePath = "/n/";
        final URI uri = URI.create(OciStorageProperties.computeServiceHost(Region.ME_JEDDAH_1) + resourcePath);
        final HttpRequest.Builder requestBuilder = HttpRequest.newBuilder(uri).GET();
        REQUEST_SIGNER
                .signRequest(uri, Method.GET.name(), Collections.emptyMap(), null)
                .entrySet().stream()
                .filter(entry -> !AUTO_FILLED_HTTP_HEADERS.contains(entry.getKey().toLowerCase()))
                .forEach(entry -> requestBuilder.header(entry.getKey(), entry.getValue()));
        final HttpResponse<String> response = CLIENT.send(requestBuilder.build(), HttpResponse.BodyHandlers.ofString());
        System.out.println(response.statusCode());
        System.out.println(response.body());
    }

    private static void listBuckets() throws IOException, InterruptedException {
        final String resourcePath = "/n/" + OciProperties.NAMESPACE + "/b?compartmentId=" + OciProperties.COMPARTMENT;
        final URI uri = URI.create(OciStorageProperties.computeServiceHost(Region.ME_JEDDAH_1) + resourcePath);
        final HttpRequest.Builder requestBuilder = HttpRequest.newBuilder(uri).GET();
        REQUEST_SIGNER
                .signRequest(uri, Method.GET.name(), Collections.emptyMap(), null)
                .entrySet().stream()
                .filter(entry -> !AUTO_FILLED_HTTP_HEADERS.contains(entry.getKey().toLowerCase()))
                .forEach(entry -> requestBuilder.header(entry.getKey(), entry.getValue()));
        final HttpResponse<String> response = CLIENT.send(requestBuilder.build(), HttpResponse.BodyHandlers.ofString());
        System.out.println(response);
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    public static final class CreateBucketRequest {
        private String compartmentId;
        private String name;
    }

}
