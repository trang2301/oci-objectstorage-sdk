package ttrang2301.oci.objectstorage.sdk;

import com.oracle.bmc.objectstorage.ObjectStorage;
import com.oracle.bmc.objectstorage.ObjectStorageClient;
import com.oracle.bmc.objectstorage.requests.PutObjectRequest;
import com.oracle.bmc.objectstorage.transfer.UploadConfiguration;
import com.oracle.bmc.objectstorage.transfer.UploadManager;
import ttrang2301.oci.OciProperties;

import java.io.File;

public class UploadObjectService {

    private static final ObjectStorage CLIENT;
    public static final String BUCKET_INVOICE_CSV_STAGING = "invoice-csv-staging";
    public static final String BUCKET_MERCHANT_SETTLEMENT_DEV_1 = "merchant-settlement-dev1";
    public static final String BUCKET_MERCHANT_SETTLEMENT_DEV_2 = "merchant-settlement-dev2";
    public static final String BUCKET = BUCKET_INVOICE_CSV_STAGING;

    static {
        CLIENT = ObjectStorageClient
                .builder()
//                .build(InstancePrincipalsAuthenticationDetailsProvider.builder().build());
                .build(OciProperties.AUTHENTICATION_DETAILS_PROVIDER);
        CLIENT.setRegion(com.oracle.bmc.Region.AP_SINGAPORE_1);
    }

    public static void main(String[] args) {
        final var configuration = UploadConfiguration
                .builder()
                .allowMultipartUploads(true)
                .build();
        final var uploadManager = new UploadManager(CLIENT, configuration);
        // TODO
        uploadManager.upload(UploadManager.UploadRequest.builder(new File("")).build(
                PutObjectRequest.builder()
                        .bucketName(BUCKET)
                        .objectName("")
                        .build()
        ));
        System.exit(1);
    }

//    public static void main(String[] args) {
//        // Prepare the parts to be uploaded
//        List<CompletedPart> completedParts = new ArrayList<>();
//        int partNumber = 1;
//        ByteBuffer buffer = ByteBuffer.allocate(5 * 1024 * 1024); // Set your preferred part size (5 MB in this example)
//
//        // Read the file and upload each part
//        try (RandomAccessFile file = new RandomAccessFile(filePath, "r")) {
//            long fileSize = file.length();
//            long position = 0;
//
//            while (position < fileSize) {
//                file.seek(position);
//                int bytesRead = file.getChannel().read(buffer);
//
//                buffer.flip();
//                UploadPartRequest uploadPartRequest = UploadPartRequest.builder()
//                        .bucket(existingBucketName)
//                        .key(keyName)
//                        .uploadId(uploadId)
//                        .partNumber(partNumber)
//                        .contentLength((long) bytesRead)
//                        .build();
//
//                UploadPartResponse response = s3.uploadPart(uploadPartRequest, RequestBody.fromByteBuffer(buffer));
//
//                completedParts.add(CompletedPart.builder()
//                        .partNumber(partNumber)
//                        .eTag(response.eTag())
//                        .build());
//
//                buffer.clear();
//                position += bytesRead;
//                partNumber++;
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

}
