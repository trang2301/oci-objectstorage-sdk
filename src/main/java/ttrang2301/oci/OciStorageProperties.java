package ttrang2301.oci;

import com.oracle.bmc.Region;
import ttrang2301.oci.OciProperties;

public class OciStorageProperties {

    public static final String SERVICE_NAME = "objectstorage";
    public static String computeServiceHost(Region region) {
        return OciProperties.computeServiceHost(SERVICE_NAME, region.getRegionId());
    }

}
