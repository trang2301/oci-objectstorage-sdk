package ttrang2301.oci;

import com.oracle.bmc.ConfigFileReader;
import com.oracle.bmc.Region;
import com.oracle.bmc.auth.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Supplier;

public class OciProperties {
    public static final String NAMESPACE = "axbjn4gpueto";
    /**
     * https://console.me-jeddah-1.oraclecloud.com/identity/compartments
     * abdulmohsen (root)
     */
    public static final String COMPARTMENT_ROOT = "ocid1.tenancy.oc1..aaaaaaaaxbrsclntcepxuktuwjfdfsx2z5v32kyyc7whexqffjdfgpscebqq";
    public static final String COMPARTMENT_STAGING = "ocid1.compartment.oc1..aaaaaaaaw2nxwfs3z5rwvlpavbhein45uwk4ls7rwea6kjydpsm2qruwzygq";
    public static final String COMPARTMENT_STAGING_SG = "ocid1.compartment.oc1..aaaaaaaa36qwdaid7kzmrf2bohuz36y4jyetx75naakygaqjjdhi7l4oxlzq";
    public static final String COMPARTMENT = COMPARTMENT_STAGING_SG;

    private static final String TENANCY = COMPARTMENT_ROOT;
    private static final String USER = "ocid1.user.oc1..aaaaaaaaby4kh2zmon4k65j6ajd4yzcjpzfgp4gim4zxvymwpozpujyxi7pa";
    private static final String FINGERPRINT = "25:19:87:46:a7:3e:b8:9e:4a:c8:80:f4:c7:9c:aa:29";
    private static final String API_HOST = "oraclecloud.com";
    private static final String API_HOST_PATTERN = "https://%s.%s." + API_HOST;
    private static final String PEM_FILE_PATH = "/Users/ttrang2301/.oci/oci_api_key.pem";

    public static String computeServiceHost(String serviceName, String region) {
        return String.format(API_HOST_PATTERN, serviceName, region);
    }

//    public static final AuthenticationDetailsProvider AUTHENTICATION_DETAILS_PROVIDER = buildAuthenticationDetailsProvider();
    public static final BasicAuthenticationDetailsProvider AUTHENTICATION_DETAILS_PROVIDER = buildAuthenticationDetailsProvider();

    private static AuthenticationDetailsProvider buildAuthenticationDetailsProvider() {
        // TODO
        final ConfigFileReader.ConfigFile configFile;
        try {
            configFile = ConfigFileReader.parseDefault();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return new ConfigFileAuthenticationDetailsProvider(configFile);
//        final Supplier<InputStream> privateKeySupplier = new SimplePrivateKeySupplier(PEM_FILE_PATH);
//        return SimpleAuthenticationDetailsProvider.builder()
//                .tenantId(TENANCY)
//                .region(Region.AP_SINGAPORE_1)
//                .userId(USER)
//                .privateKeySupplier(privateKeySupplier)
//                .fingerprint(FINGERPRINT)
//                .build();
    }
}
