package ttrang2301.oci.identity;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oracle.bmc.auth.AuthenticationDetailsProvider;
import com.oracle.bmc.auth.BasicAuthenticationDetailsProvider;
import com.oracle.bmc.http.signing.DefaultRequestSigner;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ttrang2301.oci.OciIdentityProperties;
import ttrang2301.oci.OciProperties;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * https://docs.public.oneportal.content.oci.oraclecloud.com/en-us/iaas/api/#/en/identity/20160918/
 */
public class JdkOciRegionClient {

    public static void main(String[] args) throws Exception {
        final BasicAuthenticationDetailsProvider authenticationDetailsProvider = OciProperties.AUTHENTICATION_DETAILS_PROVIDER;
        listRegionsWithJdkClient(authenticationDetailsProvider);
    }

    private static void listRegionsWithJdkClient(BasicAuthenticationDetailsProvider authenticationDetailsProvider) throws IOException, InterruptedException {
        final URI uri = URI.create(
                OciIdentityProperties.computeServiceHost(com.oracle.bmc.Region.ME_JEDDAH_1)
                + "/20160918/regions"
        );
        final HttpRequest.Builder requestBuilder = HttpRequest.newBuilder(uri).GET();
        DefaultRequestSigner.createRequestSigner(authenticationDetailsProvider)
                .signRequest(uri, "GET", Collections.emptyMap(), null)
                .entrySet()
                .stream()
                .filter(entry -> !Objects.equals("host", entry.getKey().toLowerCase()))
                .forEach(entry -> requestBuilder.header(entry.getKey(), entry.getValue()));
        final HttpRequest request = requestBuilder.build();
        final HttpResponse<String> response = HttpClient.newBuilder().build().send(request, HttpResponse.BodyHandlers.ofString());
        final List<Region> regions = new ObjectMapper().readValue(response.body(), new TypeReference<List<Region>>() {});
        regions.forEach(region -> System.out.println("key=" + region.key + ", name=" + region.name));
        System.out.println(response.body());
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    public static final class Region {
        private String key;
        private String name;
    }

}
