package ttrang2301.oci.identity;

import com.oracle.bmc.auth.AuthenticationDetailsProvider;
import com.oracle.bmc.auth.BasicAuthenticationDetailsProvider;
import com.oracle.bmc.identity.IdentityClient;
import com.oracle.bmc.identity.requests.ListRegionsRequest;
import com.oracle.bmc.identity.responses.ListRegionsResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ttrang2301.oci.OciProperties;

public class SdkOciRegionClient {

    public static void main(String[] args) throws Exception {
        listRegionsWithSdkClient(OciProperties.AUTHENTICATION_DETAILS_PROVIDER);
    }

    private static void listRegionsWithSdkClient(BasicAuthenticationDetailsProvider authenticationDetailsProvider) {
        final IdentityClient identityClient =
                IdentityClient.builder()
                        .region(com.oracle.bmc.Region.ME_JEDDAH_1)
                        .build(authenticationDetailsProvider);
        System.out.println("Querying for list of regions via the Identity Client");
        final ListRegionsResponse response =
                identityClient.listRegions(ListRegionsRequest.builder().build());
        System.out.println("List of regions: " + response.getItems());
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    public static final class Region {
        private String key;
        private String name;
    }

}
